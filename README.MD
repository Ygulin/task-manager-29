# TASK MANAGER

## DEVELOPER INFO

* **NAME**: Yurii Gulin

* **E-MAIL**: ygulin@tsconsulting.com

* **E-MAIL**: ygulin@yandex.ru

## SOFTWARE

* OpenJDK 8

* Intellij Idea

* Windows 10

## HARDWARE

* **RAM**: 16 Gb

* **CPU**: i7

* **MDD**: 512 Gb

## RUN APPLICATION

```shell
mvn clean install
```

## RUN APPLICATION

```shell
java -jar ./task-manager.jar
```
