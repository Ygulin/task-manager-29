package ru.tsc.gulin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findOneByLogin(@NotNull String login);

    @Nullable
    User findOneByEmail(@NotNull String email);

    @Nullable
    User removeByLogin(@NotNull String login);

}
